const GainAndLossCalculator = require('../src/GainAndLossCalculator')


describe('GainAndLossCalculator', () => {
  test('should calculate gains and losses given a amounts', () => {
    let amounts = [78.41, 85.18, 91.09, 90.57, 91.02, 103.61, 105.88, 103.77, 110.13, 108.89, 105.09]
    calculator = new GainAndLossCalculator()
    amounts.forEach(n => calculator.calculate(n))
    expect(calculator.printAmounts()).toEqual("Gains: 34.35, Losses: -7.67")
  })
})
