const readline = require('readline')
const GainAndLossCalculator = require('./GainAndLossCalculator')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
const gainAndLossCalculator = new GainAndLossCalculator()
const commandPrompt = () => {
  rl.question('Please enter an amount \n', (input) => {
    if(input === 'EXIT') {
      console.log('Bye')
      rl.close()
    }
    parsedNumber = parseFloat(input).toFixed(2)
    gainAndLossCalculator.calculate(parsedNumber)
    console.log(gainAndLossCalculator.printAmounts())
    commandPrompt()
  })
}

commandPrompt()