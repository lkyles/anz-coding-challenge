module.exports = class GainAndLossCalculator {
  constructor() {
    this.lastNumber = 0
    this.listOfLosses = []
    this.listOfGains = []
  }

  calculate(newAmount) {
    if(this.lastNumber) {
      if(this.lastNumber > newAmount) {
        this.listOfLosses.push((this.lastNumber - newAmount))
      } else {
        this.listOfGains.push((newAmount - this.lastNumber))
      }
    }
    this.lastNumber = newAmount
  }

  printAmounts() {
    return `Gains: ${this.sumList(this.listOfGains).toFixed(2)}, Losses: -${this.sumList(this.listOfLosses).toFixed(2)}`
  }

  sumList(list) {
    return list.reduce((acc, curr) => (acc += curr), 0.00)
  }

}