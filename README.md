#### ANZ Coding Exercise

This program print out the sums of all gains and losses. Given a the entry of amounts.

This program was developed using NodeJS

#### Running the program
```
npm start
```

#### Testing the program

Given the short time period the only test is the scenario given in the challenge

```
npm install
npm test
```

#### Assumptions

Each number is entered individually (resulting in a print of results after each)

For the purposes of this challenge all values entered are expected to be floating point values with 2 decimal places

#### Caveats

Using JS to calculate numbers with precision is not ideal but for this challenge and given the listed assumptions it should be enough
